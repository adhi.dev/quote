//
//  AppDelegate.swift
//  Quote
//
//  Created by Adhithyan V on 17/03/16.
//  Copyright © 2016 Adhithyan V. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var window: NSWindow!

    let statusItem = NSStatusBar.systemStatusBar().statusItemWithLength(-2)
    let popover = NSPopover()
    
    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
        
        if let button = statusItem.button{
            button.image = NSImage(named: "StatusBarButtonImage")
            button.action = Selector("togglePopover:")
        }
        popover.contentViewController = QuotesViewController(nibName: "QuotesViewController", bundle: nil)

    
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }

    func printQuote(sender: AnyObject){
        let quoteText = "Move fast and break things. Unless you are breaking things, you are not moving fast enough."
        let quoteAuthor = "Mark Zuckerberg"
        
        print("\(quoteText) - \(quoteAuthor)")
    }
    
    func showPopover(sender: AnyObject?){
        if let button = statusItem.button{
            popover.showRelativeToRect(button.bounds, ofView: button, preferredEdge: NSRectEdge.MinY)
        } 
    }
    
    func closePopover(sender: AnyObject?){
        popover.performClose(sender)
    }
    
    func togglePopover(sender: AnyObject?){
        if(popover.shown){
            closePopover(sender)
        }else{
            showPopover(sender)
        }
    }

}

